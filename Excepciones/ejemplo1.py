"""Crea un programa que reciba un número del 1 al 20 introducido por el usuario y compruebe si está
dentro de la siguiente lista: [6,14,11,3,2,1,15,19]. Implementa una función que se asegure que el
número introducido por el usuario está en el rango indicado y otra que compruebe si está dentro
de la lista. Trata de crear las funciones de forma que puedan ser reutilizadas lo máximo posible en
otros programas."""


def pedir_valor():
    valor=0
    seguir=True
    while seguir:
        try:
            valor = int(input("Digite Numero Mayor a Cero: "))
            # seguir = False
            break
        except ValueError:
            print("No se permiten Caracteres")
        
        print("Intente de nuevo")
    return valor

def validar_rango(valor,minimo=1,maximo=20):
    if valor>=minimo and valor<=maximo:
        return True
    else:
        return False


def en_lista(valor,lista = [6,14,11,3,2,1,15,19]):
    # lista = [6,14,11,3,2,1,15,19]
    if valor in lista:
        return True
    
    return False

def main():
    dato = pedir_valor()
    if dato==0:
        return

    if validar_rango(dato,5,15):
        if en_lista(dato,[6,7,8,9,10]):
            print("El Valor ",dato," está en la lista")
        else:
            print("El Valor ",dato," NO está en la lista")
    else:
        print("El Valor ",dato," no está en el rango")


main()