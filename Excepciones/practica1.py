"""
Crear un programa que se ingrese cuántos datos procesar
de cada dato, debe pedir nombre y edad.
Al final debe imprimir la lista (nombre y edad) digitados por el usuario
"""
import os

def limpiarPantalla():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")



def pedir_cantidad():

    cantidad = -1
    while cantidad!=0:
        try:
            cantidad = int(input("Cantidad de Personas: "))
            if cantidad<0:
                raise ValueError('')
            break
        except ValueError:
            limpiarPantalla()
            print("Digite Valor Correcto de Personas")
    
    return cantidad

def pedir_edad(nombre):
    edad = 0
    while True:
        try:
            limpiarPantalla()
            print("Nombre:", nombre)
            edad = int(input("Introduzca la Edad:"))
            break
        except ValueError:
            limpiarPantalla()
            print("Digite Valor Correcto para Edad")
    return edad

def pedir_datos():
    edad = 0
    while True:
        nombre = input("Digite Nombre:")

        if len(nombre)>0:
            edad = pedir_edad(nombre)
            break

    return {"nombre":nombre,"edad":edad}


def imprimir(datos):
    for i in datos:
        print(i["nombre"],i["edad"])

def main():
    limpiarPantalla()
    lista = []
    cantidad = pedir_cantidad()
    for i in range(cantidad):
        persona = pedir_datos()
        lista.append(persona)
    imprimir(lista)
        
        
main()

