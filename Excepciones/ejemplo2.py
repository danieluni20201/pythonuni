def dividir():
    operacion = 0
    try:
        print("Iniciando Operación")
        dividendo = int(input("Digite Dividendo: "))
        divisor = int(input("Digite Divisor: "))
        operacion = dividendo / divisor
        
    except ValueError:
        print("Sólo se admiten valores Enteros. No se admiten letras")
    except ZeroDivisionError:
        print("El divisor no puede ser cero")
    finally:
        print("Finalizando Operación")
    
    # print(50/0)
    # raise NameError('¡Soy una excepción!')
    return operacion

try:
    res = dividir()
    print(res)
except ZeroDivisionError:
    print("El divisor no puede ser cero")
# except:
#     print("Error Inesperado")

