"""Crea un programa que acceda a la posición que el usuario indique de la siguiente lista: [6,14,11,3,2,1,15,19].
No olvides capturar las excepciones que puedan surgir en caso de que el usuario introduzca un índice
incorrecto o que no exista en la lista."""


lista = [6,14,11,3,2,1,15,19]
indice = -1
try:
    indice = int(input("Digite ìndice:"))
except:
    print("Sólo se permiten valores enteros")


if indice==-1:
    pass
else:
    try:
        valor = lista[indice]
        print(valor)
    except IndexError:
        print("Valor está fuera del rango")
    finally:
        print("Proceso Terminado")
