estudiantes={}
media = 0
aprobados = []
reprobados = []
# Parte 1 - Captura de Datos
for i in range(10):
    nombre = input("Nombre: ")
    nota = float(input("Nota: "))

    diccionario = {"nombre":nombre,"nota":nota}
    
    estudiantes[i+1]=diccionario
    # print(estudiantes)
    media += nota
    # media = media + nota

    if nota>=7:
        aprobados.append(diccionario)
    else:
        reprobados.append(diccionario)

    salir = input("¿Grabar Otro? (S/N) ")
    if salir.upper()=="N":
        break


# Parte 2 - Procesar Datos
media = media / (i+1)


# print(aprobados)
# print(reprobados)

print("\n**************** RESUMEN ****************")
print("*"*3,"Cantidad de Alumnos Evaluados ",i+1)
print("*"*3,"Promedio ",media)
print("*"*3,"Aprobados")
for a in aprobados:
    print("*"*3,"\t -",a["nombre"]," (",a["nota"],")" )
print("*"*3,"Reprobados")
for a in reprobados:
    print("*"*3,"\t -",a["nombre"]," (",a["nota"],")" )
print("\n*****************************************")