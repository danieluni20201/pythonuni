"""Crea un programa que recorra una lista y cree un diccionario que contenga el número de veces que
aparece cada número en la lista.
• Ejemplo: [12, 23, 5, 12, 92, 5,12, 5, 29, 92, 64,23]
• Resultado: {12: 3, 23: 2, 5: 3, 92: 2, 29: 1, 64: 1}
             {12: 3, 23: 2, 5: 3, 92: 2, 29: 1, 64: 1}
"""

lista = [12, 23, 5, 12, 92, 5,12, 5, 29, 92, 64,23]
salida = {}
salida2 = {}

for numero in lista:
    veces = lista.count(numero)
    print(numero,veces)
    salida[numero] = veces
    print(numero, salida)
    salida2.update({numero:veces})


print(salida)
print(salida2)