"""Recorre un diccionario y crea una lista solo con los valores que contiene, sin añadir valores
duplicados.
• Ejemplo: {'Mikel': 3, 'Ane': 8, 'Amaia': 12, 'Unai': 5, 'Jon': 8, 'Ainhoa': 7,
'Maite': 5}
• Resultado: [3, 8, 12, 5, 7]
"""


diccionario = {'Mikel': 3, 'Ane': 8, 'Amaia': 12, 'Unai': 5, 'Jon': 8, 'Ainhoa': 7,'Maite': 5}
lista = []

for llave in diccionario:
    valor = diccionario[llave]
    print(llave,valor)
    if valor in lista:
        continue

    lista.append(valor)

print(lista)

print("***********************")
lista = []

for llave in diccionario:
    valor = diccionario[llave]
    # print(llave,valor)
    print(valor,lista,valor in lista,not valor in lista)

    if not valor in lista:
        lista.append(valor)

print(lista)

print("***********************")
lista = []

for valor in diccionario.values():
    # print(valor)
    print(valor,lista,valor in lista,valor not in lista)
    if valor not in lista:
        lista.append(valor)

print(lista)