estudiantes={}
media = 0

# Parte 1 - Captura de Datos
for i in range(10):
    nombre = input("Nombre: ")
    nota = float(input("Nota: "))

    estudiantes[i+1]={"nombre":nombre,"nota":nota}
    # print(estudiantes)
    media += nota
    # media = media + nota

    salir = input("¿Grabar Otro? (S/N) ")
    if salir.upper()=="N":
        break


# Parte 2 - Procesar Datos
media = media / (i+1)
aprobados = []
reprobados = []

for j in estudiantes:
    diccionario = {"nombre":estudiantes[j]["nombre"],"nota":estudiantes[j]["nota"]}
    if estudiantes[j]["nota"]>=7:
        aprobados.append(diccionario)
    else:
        reprobados.append(diccionario)

# print(aprobados)
# print(reprobados)

print("\n**************** RESUMEN ****************")
print("*"*3,"Cantidad de Alumnos Evaluados ",i+1)
print("*"*3,"Promedio ",media)
print("*"*3,"Aprobados")
for a in aprobados:
    print("*"*3,"\t -",a["nombre"]," (",a["nota"],")" )
print("*"*3,"Reprobados")
for a in reprobados:
    print("*"*3,"\t -",a["nombre"]," (",a["nota"],")" )
print("\n*****************************************")