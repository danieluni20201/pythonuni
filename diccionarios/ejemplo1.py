es = {
    "nombre": "Iñaki Perurena",
    "edad": 30,
    "nota_media": 7.25,
    "repetidor" : False,
}

print(es)

print(es["nombre"])
print(es.get("edad"))

es["nombre"] = "Juan"
print(es)
es["apellido"] = "Perez"
print(es)
es.update({'aprobados':'8'})
print(es)
es.update({'aprobados':7})
print(es)
es.update({'reprobados':2,"ausente":7})
print(es)
es.update({'reprobados':2,"ausente":1000})
print(es)

print(es.keys())
print(es.values())

print(es)
print(es.pop("ausente"))
print(es)

print("ausente" in es)
print("aprobados" in es)

print("Juan" in es.values())

print("*******************************")
for key in es:
    print(key,":",es[key])

print("*******************************")
for key in es.keys():
    print(key)

print("*******************************")
for llave,valor in es.items():
    print(llave,":",valor)

# print(es.items())

del es["reprobados"]
print(es)

lista = [1,2,3]
print(lista.count(2))

