def crearArchivo(nombre):
    archivo = open(nombre,"w")
    try:
        dato = input("Escriba algo: ")
        archivo.write(dato)
    except:
        print("Error")
    finally:
        archivo.close()

def leerArchivo(nombre):
    linea = ""
    try:
        archivo = open(nombre)
        linea = archivo.readline()
        while linea!="":
            print(linea)
            linea = archivo.readline()
    except FileNotFoundError:
        print("Archivo NO EXISTE")
    except:
        print("Sucedió un Error")
    
    return linea

# crearArchivo("archivo.txt")
leerArchivo("archivo2.txt")


    
    

