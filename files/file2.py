"""
Crear un programa que se ingrese cuántos datos procesar
de cada dato, debe pedir nombre y edad.
Al final debe imprimir la lista (nombre y edad) digitados por el usuario
"""
import os
import json

def limpiarPantalla():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")



def pedir_cantidad():

    cantidad = -1
    while cantidad!=0:
        try:
            cantidad = int(input("Cantidad de Personas: "))
            if cantidad<0:
                raise ValueError('')
            break
        except ValueError:
            limpiarPantalla()
            print("Digite Valor Correcto de Personas")
    
    return cantidad

def pedir_edad(nombre):
    edad = 0
    while True:
        try:
            limpiarPantalla()
            print("Nombre:", nombre)
            edad = int(input("Introduzca la Edad:"))
            break
        except ValueError:
            limpiarPantalla()
            print("Digite Valor Correcto para Edad")
    return edad

def pedir_datos():
    edad = 0
    while True:
        nombre = input("Digite Nombre:")

        if len(nombre)>0:
            edad = pedir_edad(nombre)
            break

    return {"nombre":nombre,"edad":edad}


def imprimir(nombre):
    archivo = open(nombre)
    linea = archivo.readline()
    while linea!="":
        linea = linea.rstrip("\\n")
        # print(linea)
        # print(type(linea))
        # persona = dict(eval(linea))
        persona = json.loads(linea)
        # print(type(persona))
        print(persona["nombre"],persona["edad"])

        linea = archivo.readline()
    # for i in datos:
    #     print(i["nombre"],i["edad"])

def guardar(nombre,persona):
    archivo = open(nombre,"a")
    # archivo.write(persona)
    archivo.writelines(persona)
    archivo.close()

def limpiarArchivo(nombre):
    archivo = open(nombre,"w")
    archivo.close()

def main():
    nombre="prueba.txt"
    limpiarArchivo(nombre)
    limpiarPantalla()
    lista = []
    cantidad = pedir_cantidad()
    for i in range(cantidad):
        persona = pedir_datos()
        guardar(nombre,json.dumps(persona)+"\n")
    limpiarPantalla()
    imprimir(nombre)
        
        
# main()
imprimir("prueba.txt")

