import os
global usuarios

usuarios = {
    "daniel": {
        "nombre": "Daniel",
        "apellido": "Bojorge",
        "password": "123123"
    },
    "fmuguruza": {
        "nombre": "Fermín",
        "apellido": "Muguruza",
        "password": "654321"
    },
    "aolaizola": {
        "nombre": "Aimar",
        "apellido": "Olaizola",
        "password": "123456"
    }
}


def clear():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")
    print(os.name)

def pedir_datos():
    usr = input("Usuario: ")
    pwd = input("Clave: ")
    return {"usr":usr,"pwd":pwd}

def validar_usuario(datos):
    usr = datos["usr"]
    pwd = datos["pwd"]

    for user in usuarios:
        if usr in usuarios and pwd == usuarios[usr]["password"]:
            usuario_valido(user)
            return True
    return False

def usuario_valido(user):
    print(usuarios[user]["nombre"],usuarios[user]["apellido"])


def main():
    for i in range(3):
        clear()
        print("Intento No. ",i+1)
        datos = pedir_datos()
        if validar_usuario(datos):
            break
    else:
        print("Cantidad de Intentos Fallidos Excedido")

main()
