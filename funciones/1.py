def saludo(nombre):
    print("Hola {}. Bienvenido!".format(nombre))

def saludo2(nombre="Anónimo"):
    print("Hola {}. Bienvenido!".format(nombre))

# saludo("Juan")
# saludo2()
# saludo2("Pedro")


def suma(a,b):
    resultado = a + b
    print(resultado)


# suma(5,25)
# suma(b=25,a=5)


def dividir(a,b):
    if b==0:
        print("No hay división")
    else:
        restultado = a/b
        print(restultado)

# print("final")

# dividir(8,0)
# dividir(b=2,a=8)
# dividir(8,2)


def sumar_varios(*args):
    # print(args)
    for a in args:
        print(a)
    print("************")



# sumar_varios()
# sumar_varios(1)
# sumar_varios(1,2)
# sumar_varios(1,2,3)

def sumar3(a,b):
    resultado = a + b
    return resultado


c = sumar3(15,20)

# suma(c,85)


def suma_todo(*args):
    resultado = 0
    for i in args:
        resultado += i
        
    return resultado

v, w, x, y, z = 5, 2, 12, 6, 9
total = suma_todo(v, w, x, y, z)
print("La suma total es:" + str(total)) # La suma total es: 34