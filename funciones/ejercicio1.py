def es_par(numero):
    if numero%2==0:
        return True
    return False

def captura():
    # valor = int(input("Digite un Valor: "))
    # return valor
    return int(input("Digite un Valor: "))

def es_primo(numero):
    for n in range(2, numero):
        if numero%n == 0:
            return False
        
    return True
    

def main():
    valor = captura()
    if es_par(valor):
        print("Es Par")
    else:
        print("Es Impar")
    
    if es_primo(valor):
        print("Es Primo")
    else:
        print("No es Primo")


main()
