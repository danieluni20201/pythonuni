import os
global usuarios

usuarios = {
    "daniel": {
        "nombre": "Daniel",
        "apellido": "Bojorge",
        "password": "123123"
    },
    "fmuguruza": {
        "nombre": "Fermín",
        "apellido": "Muguruza",
        "password": "654321"
    },
    "aolaizola": {
        "nombre": "Aimar",
        "apellido": "Olaizola",
        "password": "123456"
    }
}


def clear():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

def pedir_datos():
    usr = input("Usuario: ")
    pwd = input("Clave: ")
    return usr,pwd

def validar_usuario(usr,pwd):
    for user in usuarios:
        if usr in usuarios and pwd == usuarios[usr]["password"]:
            usuario_valido(user)
            return True
    return False

def usuario_valido(user):
    print(usuarios[user]["nombre"],usuarios[user]["apellido"])


def main():
    for i in range(3):
        clear()
        print("Intento No. ",i+1)
        usr,pwd = pedir_datos()
        if validar_usuario(usr,pwd):
            break
    else:
        print("Cantidad de Intentos Fallidos Excedido")

main()
