# usuario_valido = "daniel"
# clave_valida   = "123"

# usuario = input("Usuario: ")
# clave = input("Clave: ")

# if usuario_valido==usuario:
#     print("Usuario Correcto")
# else:
#     print("Usuario Incorrecto")

# if usuario_valido==usuario:
#     print("Usuario Correcto")
#     print("Bienvenido")
# else:
#     print("Usuario Incorrecto")
#     usuario = input("Usuario: ")
#     if usuario == usuario_valido:
#         print("Ahora si")
#     else:
#         print("Error Nuevamente")


# if usuario_valido==usuario:
#     print("Usuario Correcto")
# elif usuario=="admin":
#     print("Hola Administrador")
# elif usuario=="jorge":
#     print("Hola Jorge")
# else:
#     print("Usuario Incorrecto")

# if usuario==usuario_valido and clave==clave_valida:
#     print("Usuario Vàlido")
# elif usuario==usuario_valido and clave!=clave_valida:
#     print("Clave Incorrecta")
# else:
#     print("Datos no vàlidos")

# if usuario==usuario_valido or clave==clave_valida:
#     print("Usuario Vàlido")
# elif usuario==usuario_valido and clave!=clave_valida:
#     print("Clave Incorrecta")
# else:
#     print("Datos no vàlidos")

valor1 = int(input("Valor 1: "))
valor2 = int(input("Valor 2: "))

if valor1==0 and valor2==0:
    print("Valor No puede ser CERO")
elif valor2==0:
    print("Valor 2 no puede ser CERO")
elif valor1<0 or valor2<0:
    print("Valores no pueden ser negativo")
elif valor2>valor1:
    print("Valor 2 no puede ser mayor a valor 1")
else:
    suma = valor1 + valor2
    resta = valor1 - valor2
    mult  = valor1 * valor2
    div   = valor1 / valor2

    print("**************************")
    print("Valor1: {}\nValor2: {}".format(valor1,valor2))
    print("Suma ",suma)
    print("Resta ",resta)
    print("Multiplicaciòn ",mult)
    print("Divisiòn",div)
    print("**************************")