import json

from librerias import limpiarPantalla
from archivo import imprimir,guardar,limpiarArchivo
from app import pedir_cantidad,pedir_edad,pedir_datos



def main():
    nombre="prueba.txt"
    limpiarArchivo(nombre)
    limpiarPantalla()
    lista = []
    cantidad = pedir_cantidad()
    for i in range(cantidad):
        persona = pedir_datos()
        guardar(nombre,json.dumps(persona)+"\n")
    limpiarPantalla()
    imprimir(nombre)


main()