numero = float(input("Número: "))

if numero>0:
    print("{} es un número positivo".format(numero))
elif numero==0:
    print("{} es igual a cero".format(numero))
else:
    print("{} es un número negativo".format(numero))