# # numeros = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
# numeros = [5,6,8,4,5,6,7,10,50,1,3,23,7,18,25]
# tabla = int(input("¿La tabla del? "))

# for n in numeros:
#     if n % 2 == 0:
#         print("{} es par".format(n))
#         continue
#     m = tabla * n
#     print("{} x {} = {}".format(tabla,n,m))


numeros = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
tabla = int(input("¿La tabla del? "))
acumulado = 0
for n in numeros:
    m = tabla * n
    acumulado = acumulado + m
    print("{} x {} = {}".format(n,tabla,m))
else:
    print("El acumulado total es de ",acumulado)