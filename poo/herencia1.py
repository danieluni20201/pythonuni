class OtraClase():
    peso = 0



class Dispositivo():
    identificador = ""
    marca = ""

    def __init__(self,i,m):
        self.identificador = i
        self.marca = m

    def conectar(self):
        print("¡Conectado!")

# la clase base se indica entre paréntesis
class Teclado(Dispositivo,OtraClase):
    tipo = ""

    def __init__(self,i,m,t):
        # llamada al constructor del padre
        Dispositivo.__init__(self,i,m)
        self.tipo = t
    
    # metodo de la subclase
    def pulsar_tecla(self,tecla):
        print(tecla)

    def conectar(self):
        super().conectar()
        print("Otra cosa")


t1 = Teclado("0001", "Logitech", "AZERTY")
t1.conectar()
print(t1.identificador,t1.marca,t1.tipo)
print(t1.peso)
t1.pulsar_tecla("a")