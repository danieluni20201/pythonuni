class Robot():
    x = 0
    y = 0

    def __init__(self):
        self.x = 0
        self.y = 0

    def posicion(self):
        print("({},{}) - Desde Modulo".format(self.x,self.y))

    def mueve(self,comando):
        if comando =="A":
            self.x += 1
        
        if comando =="R":
            self.x -= 1
        
        if comando =="I":
            self.y -= 1
        
        if comando =="D":
            self.y += 1

        self.posicion()

    def mover(self,cadena):
        for letra in cadena:
            self.mueve(letra)


# r = Robot()
# r.mover("AADDADIR")