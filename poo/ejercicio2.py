class Robot():
    x = 0
    y = 0

    def __init__(self):
        self.x = 0
        self.y = 0

    def posicion(self):
        print(self.x,self.y)

    def mueve(self,comando):
        if comando =="A":
            self.x += 1
        
        if comando =="R":
            self.x -= 1
        
        if comando =="I":
            self.y -= 1
        
        if comando =="D":
            self.y += 1

        self.posicion()


r = Robot()
r.mueve("A") # (1,0)
r.mueve("A") # (1,0)
r.mueve("I") # (1,0)
r.mueve("A") # (1,0)
r.mueve("D") # (1,0)
r.mueve("D") # (1,0)
r.mueve("D") # (1,0)
r.mueve("R") # (1,0)
r.mueve("I") # (1,0)