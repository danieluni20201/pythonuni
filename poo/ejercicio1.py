class Vehiculo():
    placa = ""
    marca = ""
    kilometros_recorridos = 0
    gasolina = 0

    def __init__(self,placa,marca):
        self.placa = placa
        self.marca = marca
        print("Km: {}\nGas: {}". \
            format(self.kilometros_recorridos, self.gasolina))
    
    def avanzar(self,km):
        # self.kilometros_recorridos = self.kilometros_recorridos + km
        consumo_gasolina = km*0.1
        if self.gasolina >= consumo_gasolina:
            self.kilometros_recorridos += km
            self.gasolina -= consumo_gasolina
        else:
            print("Gasolina Insuficiente. Debe Recargar por lo menos",consumo_gasolina)
        
        print("Km: {}\nGas: {}". \
            format(self.kilometros_recorridos, self.gasolina))

    def cargar(self,lt):
        self.gasolina += lt
        print("Gas: {}".format(self.gasolina))

v = Vehiculo("123456","Toyota")
v.cargar(50)
v.avanzar(100)
v.avanzar(100)
v.avanzar(100)
v.avanzar(100)
v.avanzar(200)