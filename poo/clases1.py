class Animal():
    __patas = 0
    color = "gris"
    tipo = "carnívoro"

    def mover(self):
        print("Se Mueve")
    
    def cambiar_patas(self,p):
        self.__patas = p
        


class Perro(Animal):
    raza = "Pastor Alemán"
    nombre = "firulais"

    def ladrar(self):
        print("Ladra")

    def __init__(self,n,r):
        self.raza = r
        self.nombre = n



p = Perro("tyson","chigugua")
print(p.nombre,p.raza)
print(p)
p.cambiar_patas(5)
print(p.__patas)

# p.mover()
# p.ladrar()
# print(p.nombre)
# p.nombre="Lazy"
# print(p.nombre)
