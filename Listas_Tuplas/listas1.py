alumnos = ["Ane", "Unai", "Itziar", "Mikel"]
print(alumnos)
# print(alumnos[0])
# print(alumnos[1])
# print(alumnos[2])
# print(alumnos[-1])

alumnos.append("Juan")
print(alumnos)
alumnos.append(5)
print(alumnos)

alumnos.insert(0,"Pedro")
print(alumnos)

alumnos.remove("Pedro")
print(alumnos)

print(alumnos.pop())
print(alumnos)

otra_lista = ["Nombre1","Nombre2"]
alumnos.extend(otra_lista)
print(alumnos)

for a in alumnos:
    print("\tAlumno ",a)

