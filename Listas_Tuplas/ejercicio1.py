"""
Dada la siguiente lista [12, 23, 5, 29, 92, 64] realiza las siguientes operaciones y vete mostrando
la lista resultante por pantalla:
1. Elimina el último número y añádelo al principio.
2. Mueve el segundo elemento a la última posición.
3. Añade el número 14 al comienzo de la lista.
4. Suma todos los números de la lista y añade el resultado al final de la lista.
5. Comina la lista actual con la siguiente: [4, 11, 32]
6. Elimina todos los números impares de la lista.
7. Ordena los números de la lista de forma ascendente.
8. Vacía la lista
"""

lista = [12, 23, 5, 29, 92, 64]
print(lista)
lista.insert(0,lista.pop())
print("1. Elimina el último número y añádelo al principio.")
print("\t",lista)
print("2. Mueve el segundo elemento a la última posición.")
lista.append(lista.pop(1))
print("\t",lista)
print("3. Añade el número 14 al comienzo de la lista.")
lista.insert(0,14)
print("\t",lista)
print("4. Suma todos los números de la lista y añade el resultado al final de la lista.")
suma = 0
for n in lista:
    suma = suma + n
lista.append(suma)
print("\t",lista)
print("5. Combina la lista actual con la siguiente: [4, 11, 32]")
lista.extend([4, 11, 32])
print("\t",lista)
print("6. Elimina todos los números impares de la lista.")
print("Accediendo por elemento")
lista2 = []
for n in lista:
    # print(n%2)
    if n%2==0:
        lista2.append(n)

print(lista2)

print("Accediendo por índice")
size = len(lista)
lista2 = []
# print(range(size))
for i in range(size):
    # print(lista[i])
    if lista[i]%2==0:
        lista2.append(lista[i])

lista = lista2
print("\t",lista)

print("7. Ordena los números de la lista de forma ascendente.")
lista2 = lista.sort()
print("\t",lista)
print("8. Vacía la lista")
lista.clear()
print("\t",lista)