import sqlite3

def conectar():
    bd = "basedatos.sqlite"
    conn = None
    try:
        conn = sqlite3.connect(bd)
    except:
        print("Imposible Conectarse a la Base de Datos")
    return conn

def desconectar(conn):
    conn.close()

def inicializar():   
    conn = conectar()
    if conn==None:
        print("No se Inicializó")
        return 

    try:
        cur = conn.cursor()
        cur.execute('DROP TABLE IF EXISTS Personas')
        cur.execute('CREATE TABLE Personas (id INTEGER PRIMARY KEY AUTOINCREMENT,nombre TEXT, edad INTEGER)')
    except expression as identifier:
        print("No se creó la tabla adecuadamente")
    finally:
        desconectar(conn)



def nuevo(nombre,edad):

    conn = conectar()
    if conn==None:
        print("No se Inicializó")
        return 

    try:
        cur = conn.cursor()
        sql = "insert into Personas (nombre,edad) values ('{}',{})". \
            format(nombre,edad)
        print(sql)
        cur.execute(sql)
        conn.commit()
    except:
        print("No se Insertó el Registro")
    finally:
        desconectar(conn)


def actualizar(id,nombre,edad):
    conn = conectar()
    if conn==None:
        print("No se Inicializó")
        return 

    try:
        cur = conn.cursor()
        sql = "update Personas set nombre='{}',edad={} where id={}". \
            format(nombre,edad,id)
        print(sql)
        cur.execute(sql)
        conn.commit()
    except:
        print("No se Actualizó el Registro")
    finally:
        desconectar(conn)


def devolver(id=None):
    conn = conectar()
    lista = []
    if conn==None:
        print("No se Inicializó")
        return 

    try:
        cur = conn.cursor()
        sql = ""
        if id:
            sql = "Select * from Personas where id={}". \
            format(id)
        else:
            sql = "Select * from Personas order by id"
        # print(sql)
        cur.execute(sql)
        for row in cur:
            # print(row)
            lista.append(row)
    except:
        print("No se Actualizó el Registro")
    finally:
        desconectar(conn)
    
    return lista


def borrar(id):
    conn = conectar()
    if conn==None:
        print("No se Inicializó")
        return 

    try:
        cur = conn.cursor()
        sql = "delete from Personas where id={}". \
            format(id)
        print(sql)
        cur.execute(sql)
        conn.commit()
    except:
        print("No se Insertó el Registro")
    finally:
        desconectar(conn)


def existe_persona(nombre):
    conn = conectar()
    contador=0
    if conn==None:
        print("No se Inicializó")
        return 

    try:
        cur = conn.cursor()
        sql = "select nombre from Personas where nombre='{}'". \
            format(nombre)
        
        cur.execute(sql)
        for r in cur:
            contador+=1

    except:
        print("Error Devolviendo Registros")
    finally:
        desconectar(conn)

    return contador>0