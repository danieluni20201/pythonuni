import bd
from librerias import limpiarPantalla

def menu():
    limpiarPantalla()
    print("1. Listar Todos")
    print("2. Insertar")
    print("3. Actualizar")
    print("4. Borrar")
    print("5. Salir")
    opc = int(input("Digite su Opción: 1-5:"))
    return opc


def listar_todos():
    lista = bd.devolver()

    limpiarPantalla()
    print("Id\tNombre\tEdad")
    # print(lista)
    for l in lista:
        print("{}\t{}\t{}".format(l[0],l[1],l[2]))

def insertar():
    limpiarPantalla()
    n = input("Nombre:")
    e = int(input("Edad:"))
    if not bd.existe_persona(n):
        bd.nuevo(n,e)
    else:
        print(n," ya existe")

def actualizar():
    listar_todos()
    id = input("Id a Actualizar:")
    n = input("Nombre:")
    e = int(input("Edad:"))
    bd.actualizar(id,n,e)
    


def borrar():
    listar_todos()
    id = input("Id a Borrar:")
    n = bd.devolver(id)
    if n:
        nombre = n[0][1]
        aceptar = input("¿Borrar {} (s-n)?".format(nombre))
        if aceptar.upper()=="S":
            bd.borrar(id)
    else:
        print("id no registrado en la Base de Datos")


def main():
    while True:
        opc = menu()
        if opc==1:
            listar_todos()
        elif opc==2:
            insertar()
        elif opc==3:
            actualizar()
        elif opc==4:
            borrar()
        else:
            break

        input("Presione Cualquier Tecla para continuar")

main()
