from librerias import limpiarPantalla

def pedir_cantidad():

    cantidad = -1
    while cantidad!=0:
        try:
            cantidad = int(input("Cantidad de Personas: "))
            if cantidad<0:
                raise ValueError('')
            break
        except ValueError:
            limpiarPantalla()
            print("Digite Valor Correcto de Personas")
    
    return cantidad

def pedir_edad(nombre):
    edad = 0
    while True:
        try:
            limpiarPantalla()
            print("Nombre:", nombre)
            edad = int(input("Introduzca la Edad:"))
            break
        except ValueError:
            limpiarPantalla()
            print("Digite Valor Correcto para Edad")
    return edad

def pedir_datos():
    edad = 0
    while True:
        nombre = input("Digite Nombre:")

        if len(nombre)>0:
            edad = pedir_edad(nombre)
            break

    return {"nombre":nombre,"edad":edad}