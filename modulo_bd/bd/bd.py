import sqlite3

db = "modulo_bd/bd/basedatos.sqlite"

def conectarse():
    conn = sqlite3.connect(db)
    return conn

def desconectarse(conn):
    conn.close()


def crear_tabla():
    conn = conectarse()
    cur = conn.cursor()
    cur.execute('DROP TABLE IF EXISTS Personas')
    cur.execute('CREATE TABLE Personas (nombre TEXT, edad INTEGER)')
    desconectarse(conn)


def insertar(persona):
    conn = conectarse()
    cur = conn.cursor()
    try:
        cur.execute("insert into Personas (nombre,edad) values ('{}',{})".format(persona["nombre"],persona["edad"]))
        conn.commit()
    except sqlite3.OperationalError:
        print("Fallo Insertando")
    finally:
        desconectarse(conn)


def todos():
    salida = []
    conn = conectarse()
    cur = conn.cursor()
    try:
        cur.execute("Select * from Personas")
        for row in cur:
            salida.append({"nombre":row[0],"edad":row[1]})
    except sqlite3.OperationalError:
        print("Fallo Insertando")
    finally:
        desconectarse(conn)
    
    return salida