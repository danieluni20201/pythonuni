# Escribe un programa que solicite el nombre, DNI y edad, 
# lo almacene en 3 variables distintas y
# muestre por pantalla los valores introducidos.

nombre = input("Nombre: ")
cedula = input("Cédula: ")
edad = int(input("Edad: "))

print("***************SALIDA*************")
print("Nombre\t:\t{}\nCédula\t:\t{}\nEdad\t:\t{}". \
    format(nombre,cedula,edad))
print("***************SALIDA*************")