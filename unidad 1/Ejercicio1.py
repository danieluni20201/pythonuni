# Ejercicio 1
# Escribe un programa que contenga las siguientes variables:
# - nombre: tipo string y valor “Kobe Bryant”
# - edad: tipo integer y valor 45
# - media_puntos: tipo float y valor 28.5
# - activo: False
# El programa deberá mostrar en pantalla todos los valores.

nombre = input("Nombre: ")
edad = int(input("Edad: "))
media_puntos = float(input("Media Puntos: "))
activo = False

salida = "Nombre: {}\n\tEdad: {}\nMedia Puntos: {}\nActivo:{}". \
    format(nombre,edad,media_puntos,activo)

print(salida)